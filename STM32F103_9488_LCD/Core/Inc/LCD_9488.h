
#ifndef __LCD_9488_H__
#define __LCD_9488_H__

#include "stm32f1xx.h"


extern uint32_t Paint_color;	
extern uint32_t Back_color; 

void LCD_9488_Init(void);
void LCD_Clear(uint32_t color);
void LCD_DrawPoint(uint32_t x, uint32_t y);

void LCD_WR_REG(uint8_t regval);
void LCD_WR_DATA(uint8_t data);

void delay_ms(__IO uint32_t nCount);

void ILI9488_EnterSleep(void);
void ILI9488_ExitSleep(void);

// IO Setting
//RST PA0
#define RST_PIN_LOW	 	 GPIOA->BRR  = BIT_0;
#define RST_PIN_HIGH	 GPIOA->BSRR = BIT_0;
//RS PA3
#define RS_PIN_LOW	 	 GPIOA->BRR  = BIT_3;
#define RS_PIN_HIGH	 	 GPIOA->BSRR = BIT_3;
//CS PA4
#define CS_PIN_LOW	 	 GPIOA->BRR  = BIT_4;
#define CS_PIN_HIGH	 	 GPIOA->BSRR = BIT_4;

// CMD
#define	SET_X_CMD		0x2A
#define	SET_Y_CMD		0x2B
#define	WRITE_RAM_CMD	0x2C


//18bit        last2 bit ignore
#define WHITE         	 0xFCFCFC
#define BLACK            0X000000
#define RED           	 0xFC0000
#define GREEN            0x00FC00
#define BLUE             0x0000FC


#define	BIT_0		(0x01 << 0)
#define	BIT_1		(0x01 << 1)
#define	BIT_2		(0x01 << 2)
#define	BIT_3		(0x01 << 3)
#define	BIT_4		(0x01 << 4)
#define	BIT_5		(0x01 << 5)
#define	BIT_6		(0x01 << 6)
#define	BIT_7		(0x01 << 7)

#define	BIT_8		(0x01 << 8)
#define	BIT_9		(0x01 << 9)
#define	BIT_10		(0x01 << 10)
#define	BIT_11		(0x01 << 11)
#define	BIT_12		(0x01 << 12)
#define	BIT_13		(0x01 << 13)
#define	BIT_14		(0x01 << 14)
#define	BIT_15		(0x01 << 15)

#endif //__LCD_9488_H__

