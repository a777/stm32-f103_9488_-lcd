
#include "LCD_9488.h"

uint32_t width = 320;
uint32_t height = 480;

uint32_t Paint_color = 0xFCFCFC;	
uint32_t Back_color = 0xFCFCFC; 

void LCD_9488_Init(void)
{
    //RST = 1 					
    RST_PIN_HIGH;
    delay_ms(10);
    //RST = 0
    RST_PIN_LOW;
    delay_ms(50);
    //RST = 1
    RST_PIN_HIGH;
    delay_ms(120);

	//ILI9488 + CTC3.5
//************* Start Initial Sequence **********//
	LCD_WR_REG(0xE0); 
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x07); 
	LCD_WR_DATA(0x0f); 
	LCD_WR_DATA(0x0D); 
	LCD_WR_DATA(0x1B); 
	LCD_WR_DATA(0x0A); 
	LCD_WR_DATA(0x3c); 
	LCD_WR_DATA(0x78); 
	LCD_WR_DATA(0x4A); 
	LCD_WR_DATA(0x07); 
	LCD_WR_DATA(0x0E); 
	LCD_WR_DATA(0x09); 
	LCD_WR_DATA(0x1B); 
	LCD_WR_DATA(0x1e); 
	LCD_WR_DATA(0x0f);  
	
	LCD_WR_REG(0xE1); 
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x22); 			// ALL pixels off
	LCD_WR_DATA(0x24); 
	LCD_WR_DATA(0x06); 
	LCD_WR_DATA(0x12); 
	LCD_WR_DATA(0x07); 
	LCD_WR_DATA(0x36); 
	LCD_WR_DATA(0x47); 
	LCD_WR_DATA(0x47); 
	LCD_WR_DATA(0x06); 
	LCD_WR_DATA(0x0a); 
	LCD_WR_DATA(0x07); 
	LCD_WR_DATA(0x30); 
	LCD_WR_DATA(0x37); 
	LCD_WR_DATA(0x0f); 
	
	LCD_WR_REG(0xC0); 
	LCD_WR_DATA(0x10); 
	LCD_WR_DATA(0x10); 
	
	LCD_WR_REG(0xC1); 
	LCD_WR_DATA(0x41); 
	
	LCD_WR_REG(0xC5); 
	LCD_WR_DATA(0x00); 
	LCD_WR_DATA(0x22); 
	LCD_WR_DATA(0x80); 
	
	LCD_WR_REG(0x36); 
	LCD_WR_DATA(0x48); 
	
	LCD_WR_REG(0x3A); //Interface Mode Control
	LCD_WR_DATA(0x66);
		
	LCD_WR_REG(0XB0);  //Interface Mode Control  
	LCD_WR_DATA(0x00); 
	LCD_WR_REG(0xB1);   //Frame rate 70HZ  
	LCD_WR_DATA(0xB0); 
	LCD_WR_DATA(0x11); 
	LCD_WR_REG(0xB4); 
	LCD_WR_DATA(0x02);   
	LCD_WR_REG(0xB6); //RGB/MCU Interface Control
	LCD_WR_DATA(0x02); 
	LCD_WR_DATA(0x02); 
	
	LCD_WR_REG(0xB7); 
	LCD_WR_DATA(0xC6); 
	
	//LCD_WR_REG(0XBE);
	//LCD_WR_DATA(0x00);
	//LCD_WR_DATA(0x04);
	
	LCD_WR_REG(0xE9); 
	LCD_WR_DATA(0x00);		// nop
	
	LCD_WR_REG(0XF7);    
	LCD_WR_DATA(0xA9); 
	LCD_WR_DATA(0x51); 
	LCD_WR_DATA(0x2C); 
	LCD_WR_DATA(0x82);
	
	LCD_WR_REG(0x11); 
	delay_ms(120); 
	LCD_WR_REG(0x29); 		//display on
	
//	LCD_Clear(BLACK);
	
}

void LCD_DrawPoint(uint32_t x, uint32_t y)
{
	//Setting page & col addr
	LCD_WR_REG(SET_X_CMD); 
	LCD_WR_DATA(x>>8);
	LCD_WR_DATA(x&0xFF);
	LCD_WR_DATA(x>>8);
	LCD_WR_DATA(x&0xFF);
	
	LCD_WR_REG(SET_Y_CMD); 
	LCD_WR_DATA(y>>8);
	LCD_WR_DATA(y&0xFF);
	LCD_WR_DATA(y>>8);
	LCD_WR_DATA(y&0xFF);

	// Start Write GRAM	
	LCD_WR_REG(WRITE_RAM_CMD);     		 	
	
	// data
	LCD_WR_DATA(Paint_color>>16);
	LCD_WR_DATA(Paint_color>>8);
	LCD_WR_DATA(Paint_color);
}


void LCD_Clear(uint32_t color)
{
	uint32_t i;      
	uint32_t total_point = width * height; 
	
	
	//Setting page & col addr
	LCD_WR_REG(SET_X_CMD); 
	LCD_WR_DATA(0);
	LCD_WR_DATA(0);
	LCD_WR_DATA((width-1)>>8);
	LCD_WR_DATA((width-1) & 0xFF);
	
	LCD_WR_REG(SET_Y_CMD); 
	LCD_WR_DATA(0);
	LCD_WR_DATA(0);
	LCD_WR_DATA((height-1)>>8);
	LCD_WR_DATA((height-1) & 0xFF);  
	
	//Start Write GRAM	 	
	LCD_WR_REG(WRITE_RAM_CMD);     
	
	
	CS_PIN_LOW;
	RS_PIN_HIGH;			//RS=1 DATA
		
	for(i = 0; i < total_point; i++)
	{
		
		
		SPI1->DR = color>>16;
		
		//while( (SPI1->SR & SPI_FLAG_BSY) );
		while( !(SPI1->SR & SPI_SR_TXE) );
		
		SPI1->DR = color>>8;
		
		//while( (SPI1->SR & SPI_FLAG_BSY) );
		while( !(SPI1->SR & SPI_SR_TXE) );
		
		SPI1->DR = color;
		
		//while( (SPI1->SR & SPI_FLAG_BSY) );
		while( !(SPI1->SR & SPI_SR_TXE) );
		
//		LCD_WR_DATA(color>>16);
//		LCD_WR_DATA(color>>8);
//		LCD_WR_DATA(color);	
	}
	
	CS_PIN_HIGH;
	
}  

void LCD_WR_REG(uint8_t regval)
{

    CS_PIN_LOW;
    RS_PIN_LOW;				//RS=0 CMD
	
	// 9488 4 line SPI spec max speed 20M, but >20M also can work
	SPI1->DR = regval;
	
	//while( (SPI1->SR & SPI_FLAG_BSY) );
	while( !(SPI1->SR & SPI_SR_TXE) );
    CS_PIN_HIGH;
}

void LCD_WR_DATA(uint8_t data)
{
    CS_PIN_LOW;
    RS_PIN_HIGH;			//RS=1 DATA
    
	SPI1->DR = data;
	
	//while( (SPI1->SR & SPI_FLAG_BSY) );
	while( !(SPI1->SR & SPI_SR_TXE) );
    CS_PIN_HIGH;
}


void delay_ms(__IO uint32_t nCount)
{
    uint32_t cnt;
    for(; nCount != 0; nCount--)
    {
        cnt = 11000;
        while(cnt--);
    }
}


void ILI9488_EnterSleep(void) 	
{ 	
	LCD_WR_REG(0x28); 	
	delay_ms(10); 	
	LCD_WR_REG(0x10);     	
	delay_ms(120); 	
} 	
 	
 	
void ILI9488_ExitSleep(void) 	
{ 	
	LCD_WR_REG(0x11);     	
	delay_ms(120); 	
	LCD_WR_REG(0x29); 	
} 	

